SELECT category_name, SUM(item_price) AS total_price FROM question2.item 
INNER JOIN question2.item_category ON (item.category_id = item_category.category_id) 
GROUP BY item_category.category_id ORDER BY total_price DESC;